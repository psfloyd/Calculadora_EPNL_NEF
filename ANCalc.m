function varargout = ANCalc(varargin)
% ANCALC MATLAB code for ANCalc.fig
%      ANCALC, by itself, creates a new ANCALC or raises the existing
%      singleton*.
%
%      H = ANCALC returns the handle to a new ANCALC or the handle to
%      the existing singleton*.
%
%      ANCALC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ANCALC.M with the given input arguments.
%
%      ANCALC('Property','Value',...) creates a new ANCALC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ANCalc_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ANCalc_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ANCalc

% Last Modified by GUIDE v2.5 17-Oct-2018 20:00:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @ANCalc_OpeningFcn, ...
    'gui_OutputFcn',  @ANCalc_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ANCalc is made visible.
function ANCalc_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for ANCalc
handles.output = hObject;

% Version del programa
handles.version='1.1.4';


addpath('functions')
addpath('functions/GUI')

movegui(handles.mainFigure, 'center')

if ~isprop(handles.mainFigure, 'EPNLData')
addprop(handles.mainFigure, 'EPNLData');
end

if ~isprop(handles.mainFigure, 'NEFData')
addprop(handles.mainFigure, 'NEFData');
end

handles.uInternal.bandasLabel= {'' 'Global' '50' '63' '80' '100' '125' '160' '200' '250' '315' '400' '500' '630' '800' '1000' '1250' ...
    '1600' '2000' '2500' '3150' '4000' '5000' '6300' '8000' '10000'};

set_plot_options(handles);

handles.userParams.calMode='file';
handles.userParams.avName='';
handles.userParamsNEF.EPNL=zeros(0);
handles.userParamsNEF.vDia=zeros(0);
handles.userParamsNEF.vNoc=zeros(0);
handles.userParamsNEF.vDia_A=zeros(0);
handles.userParamsNEF.vNoc_A=zeros(0);
handles.NEFData.NEF=zeros(0);
handles.NEFData.ANEF=zeros(0);


xlim(handles.axesTime, [0 30])
ylim(handles.axesTime, [0 100])
ylim(handles.axesSpectrum, [0 100])



% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ANCalc wait for user response (see UIRESUME)
% uiwait(handles.mainFigure);


function varargout = ANCalc_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;


function handles = buttonLoadCal_Callback(hObject, eventdata, handles)

[filename,path]=uigetfile( {'*.wav','Archivos WAV (*.wav)';'*.*','Todos los archivos(*.*)'}, 'Cargar Calibración:');


if ~isnumeric(filename)
    handles.userParams.calFilepath=strcat(path,filename);
    set(handles.textCalFile,'String',filename)
    
    guidata(hObject, handles);
end






function textCal_Callback(hObject, eventdata, handles)


function textCal_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editCal_Callback(hObject, eventdata, handles)


function editCal_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function handles = buttonLoadAudio_Callback(hObject, eventdata, handles)

[filename,path]=uigetfile( {'*.wav','Archivos WAV (*.wav)';'*.*','Todos los archivos(*.*)'}, 'Cargar Audio:');

if ~isnumeric(filename)
    handles.userParams.audioFilepath=strcat(path,filename);
    set(handles.textAudioFile,'String',filename)
    
    guidata(hObject, handles);
end



function textAudioFile_Callback(hObject, eventdata, handles)


function textAudioFile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function textCalFile_Callback(hObject, eventdata, handles)


function textCalFile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function editCalFactor_Callback(hObject, eventdata, handles)





function editCalFactor_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editCalFileLevel_Callback(hObject, eventdata, handles)


function editCalFileLevel_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function handles = selectCalFile_Callback(hObject, eventdata, handles)
set(handles.editCalFactor,'Enable','off')
set(handles.editCalFileLevel,'Enable','on')

set(handles.buttonLoadCal,'Enable','on')
set(handles.textCalFile,'Enable','inactive')

handles.userParams.calMode='file';
guidata(hObject, handles);


function handles = selectCalFactor_Callback(hObject, eventdata, handles)
set(handles.editCalFactor,'Enable','on')
set(handles.editCalFileLevel,'Enable','off')

set(handles.buttonLoadCal,'Enable','off')
set(handles.textCalFile,'Enable','off')

handles.userParams.calMode='factor';
guidata(hObject, handles);


function buttonProcess_Callback(hObject, eventdata, handles)
tmpAvName=get(handles.editAvName,'String');
if isempty(tmpAvName)
    errordlg('Debe especificar un nombre para la aeronave.', 'Error')
    return
else
    avName=tmpAvName;
    handles.userParams.avName=avName;
end

usingCalFile=get(handles.selectCalFile, 'Value');
usingCalFactor=get(handles.selectCalFactor, 'Value');

if usingCalFile
    % casos de error
    tmpCalSPL=get(handles.editCalFileLevel,'String');
    if isempty(tmpCalSPL)
        errordlg('Debe especificar el nivel de calibración.', 'Error')
        return
    elseif isnan(str2double(tmpCalSPL))
        errordlg('El nivel de calibración debe ser numérico', 'Error')
        return
    else
        calSPL=str2double(tmpCalSPL);
        handles.userParams.calSPL=calSPL;
    end
    
    if ~isfield(handles.userParams,'calFilepath')
        errordlg('Debe seleccionar un archivo de calibración.', 'Error')
        return
    else
        calFilepath=handles.userParams.calFilepath;
    end
    
    if ~isfield(handles.userParams,'audioFilepath')
        errordlg('Debe seleccionar un archivo de audio.', 'Error')
        return
    else
        audioFilepath=handles.userParams.audioFilepath;
        
    end
    
    
    calFactor=calc_calibracion(calFilepath, calSPL);
    
    
elseif usingCalFactor
    
    tmpCalFactor=get(handles.editCalFactor,'String');
    if isempty(tmpCalFactor)
        errordlg('Debe especificar el factor de calibración.', 'Error')
        return
    elseif isnan(str2double(tmpCalFactor))
        errordlg('El factor de calibración debe ser numérico', 'Error')
        return
    else
        calFactor=str2double(tmpCalFactor);
        handles.userParams.calFactor=calFactor;
    end
    
    if ~isfield(handles.userParams,'audioFilepath')
        errordlg('Debe seleccionar un archivo de audio.', 'Error')
        return
    else
        audioFilepath=handles.userParams.audioFilepath;
        
    end
    
end


waitBar=waitbar(0,'Procesando...');

if ~isfield(handles.uInternal,'filtrosStruct')
[ ~, procData, filtrosStruct ] = calc_EPNL( audioFilepath, calFactor );
else
[ ~, procData, filtrosStruct ] = calc_EPNL( audioFilepath, calFactor, handles.uInternal.filtrosStruct);
end
handles.uInternal.filtrosStruct=filtrosStruct;

EPNLData = results_for_show( handles, procData );
handles = show_results( handles, EPNLData );

waitBar=waitbar(1, waitBar);
close(waitBar)


guidata(hObject, handles);


function EPNLData = results_for_show( handles, procData )
EPNLData.EPNL=procData.EPNL;
EPNLData.t=procData.t;
EPNLData.timeMat=vertcat(suma_energetica(procData.SPL), procData.SPL);
EPNLData.bandasLabel=handles.uInternal.bandasLabel(2:end);
EPNLData.spectrMat=horzcat( promedio_energetico(spl2pa(EPNLData.timeMat),2), EPNLData.timeMat);
EPNLData.L_P=procData.L_P;
EPNLData.L_PN=procData.L_PN;
EPNLData.L_PNT=procData.L_PNT;

function tabla = table_for_export(handles)
tabla.EPNL{ 1,1} = handles.userParams.avName;
tabla.EPNL{ 2,1} = round(max(handles.EPNLData.L_P),1);
tabla.EPNL{ 3,1} = round(max(handles.EPNLData.L_PN),1);
tabla.EPNL{ 4,1} = round(max(handles.EPNLData.L_PNT),1);
tabla.EPNL{ 5,1} = round(handles.EPNLData.EPNL,1);

tabla.NEF{ 1,1} = round(handles.userParamsNEF.EPNL,1);
tabla.NEF{ 2,1} = round(handles.userParamsNEF.vDia,1);
tabla.NEF{ 3,1} = round(handles.userParamsNEF.vNoc,1);
tabla.NEF{ 4,1} = round(handles.NEFData.NEF,0);
tabla.NEF{ 5,1} = round(handles.userParamsNEF.vDia_A,1);
tabla.NEF{ 6,1} = round(handles.userParamsNEF.vNoc_A,1);
tabla.NEF{ 7,1} = round(handles.NEFData.ANEF,0);



function buttonResetSpectrum_Callback(hObject, eventdata, handles)
graphics_update(handles.mainFigure, 'axesTime', -1 )

function buttonResetTime_Callback(hObject, eventdata, handles)
graphics_update(handles.mainFigure, 'axesSpectrum', 1 )


function buttonSaveResult_Callback(hObject, eventdata, handles)
[filename,path]=uiputfile( {'*.mat','Archivos MAT (*.mat)'}, 'Guardar Resultados:', 'Resultados.mat');

if ~isnumeric(filename)
    waitBar=waitbar(0,'Guardando...');
    filepath=strcat(path,filename);
    EPNLData=handles.EPNLData;
    userParams=handles.userParams;
    NEFData=handles.NEFData;
    userParamsNEF=handles.userParamsNEF;
    
    save(filepath, 'userParams', 'EPNLData', 'userParamsNEF', 'NEFData')
    
    waitBar=waitbar(1, waitBar);
    close(waitBar)
end


function buttonExport_Callback(hObject, eventdata, handles)
export_main(handles);


function handles = buttonLoadResult_Callback(hObject, eventdata, handles)
handles=load_results(handles);

guidata(hObject, handles);


function handles = load_results(handles)

[filename,path]=uigetfile( {'*.mat','Archivos MAT (*.mat)'}, 'Cargar Resultados:', 'Resultados.mat');

if ~isnumeric(filename)
    waitBar=waitbar(0,'Cargando...');
    filepath=strcat(path,filename);
    tmpLoad=load(filepath);

    if ~isfield(tmpLoad,'EPNLData')
        close(waitBar)
        errordlg('Archivo no valido', 'Error')
        return
    end
    
    
    handles.EPNLData=tmpLoad.EPNLData;
    handles.userParams=tmpLoad.userParams;
    handles.NEFData=tmpLoad.NEFData;
    handles.userParamsNEF=tmpLoad.userParamsNEF;
    
    
    handles = show_params(handles, handles.userParams);
    handles = show_results(handles, handles.EPNLData);
    handles = show_params_NEF(handles, handles.userParamsNEF);
    
    waitBar=waitbar(1, waitBar);
    close(waitBar)
    
end


function handles = show_results(handles,EPNLData)
handles.EPNLData=EPNLData;

set(handles.mainFigure, 'EPNLData', handles.EPNLData);

set(handles.editEPNL, 'String', strcat(num2str(round(EPNLData.EPNL,1)),' EPNdB'))


plot(handles.axesTime, EPNLData.t, EPNLData.timeMat(1,:), EPNLData.t, EPNLData.L_PN, EPNLData.t, EPNLData.L_PNT)
bar(handles.axesSpectrum, EPNLData.spectrMat(:,1))
legend(handles.axesTime, char(strcat('Sin corr. (',EPNLData.bandasLabel(1),')')),'Corr. por ruidosidad', 'Corr. por tonos')
legend(handles.axesSpectrum, 'Total')

set_plot_options(handles)

set(handles.menuA_saveResult, 'Enable', 'on');
set(handles.menuA_exportResult, 'Enable', 'on');

set(handles.buttonSaveResult, 'Enable', 'on');
set(handles.buttonExport, 'Enable', 'on');

set(handles.buttonResetTime, 'Enable', 'on');
set(handles.buttonResetSpectrum, 'Enable', 'on');
set(handles.buttonExtGraph, 'Enable', 'on');
set(handles.buttonNEF_loadEPNL, 'Enable', 'on')



function handles = show_params_NEF(handles, userParamsNEF)
set(handles.editNEF_EPNL,'String',num2str(handles.userParamsNEF.EPNL));
set(handles.editNEF_vDia,'String',num2str(handles.userParamsNEF.vDia));
set(handles.editNEF_vNoc,'String',num2str(handles.userParamsNEF.vNoc));
set(handles.editNEF_vDia_A,'String',num2str(handles.userParamsNEF.vDia_A));
set(handles.editNEF_vNoc_A,'String',num2str(handles.userParamsNEF.vNoc_A));

handles=gui_calc_NEF(handles);


function handles=show_params(handles, userParams)
set(handles.editAvName,'String',handles.userParams.avName)

[~,audioName,audioExt]=fileparts(userParams.audioFilepath);

set(handles.textAudioFile,'String',strcat(audioName, audioExt))

switch userParams.calMode
    case 'file'
        handles=selectCalFile_Callback(handles.selectCalFile,0,handles);
        set(handles.selectCalFile,'Value',1)
        [~,calName,calExt]=fileparts(userParams.calFilepath);
        set(handles.textCalFile,'String',strcat(calName, calExt))
        
    case 'factor'
        handles=selectCalFactor_Callback(handles.selectCalFactor,0,handles);
        
        set(handles.selectCalFactor,'Value',1)
        set(handles.editCalFactor,'String',num2str(userParams.calFactor))
end



function set_plot_options(handles)
datacursor_obj=datacursormode(handles.mainFigure);
set(datacursor_obj,'UpdateFcn',@datacursor_update);
datacursormode on;

set_plot_axis(handles)


function set_plot_axis(handles)
% axesTime
set(handles.axesTime,'Tag','axesTime')

xlabel(handles.axesTime, 'Tiempo[s]')
ylabel(handles.axesTime, 'Nivel[dB SPL]')


% axesSpectrum
set(handles.axesSpectrum,'Tag','axesSpectrum')

xlabel(handles.axesSpectrum, 'Frecuencia[Hz]')
ylabel(handles.axesSpectrum, 'Nivel[dB SPL]')

xlim(handles.axesSpectrum, [0 length(handles.uInternal.bandasLabel)])
% xticks(handles.axesSpectrum, 0:1:length(handles.uInternal.bandasLabel))
% xticklabels(handles.axesSpectrum, handles.uInternal.bandasLabel)
% xtickangle(handles.axesSpectrum, 45)
set(handles.axesSpectrum, 'XTick',0:1:length(handles.uInternal.bandasLabel))
set(handles.axesSpectrum, 'XTickLabel',handles.uInternal.bandasLabel)
set(handles.axesSpectrum, 'XTickLabelRotation', 45)


function plot_external(handles)
figure;
graphs.uInternal.bandasLabel=handles.uInternal.bandasLabel;

graphs.axesTime=subplot(2,1,1);
plot(handles.EPNLData.t, handles.EPNLData.timeMat(1,:), handles.EPNLData.t, ...
handles.EPNLData.L_PN, handles.EPNLData.t, handles.EPNLData.L_PNT);
graphs.axesSpectrum=subplot(2,1,2);
bar(handles.EPNLData.spectrMat(:,1));
legend(graphs.axesTime, char(strcat('Sin corr. (',handles.EPNLData.bandasLabel(1),')')),'Corr. por ruidosidad', 'Corr. por tonos')
legend(graphs.axesSpectrum, 'Total')

suptitle(handles.userParams.avName);

set_plot_axis(graphs);


function editNEF_EPNL_Callback(hObject, eventdata, handles)
handles=gui_calc_NEF(handles);
guidata(hObject, handles);


function editNEF_EPNL_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editNEF_vDia_Callback(hObject, eventdata, handles)
handles=gui_calc_NEF(handles);
guidata(hObject, handles);


function editNEF_vDia_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editNEF_vNoc_Callback(hObject, eventdata, handles)
handles=gui_calc_NEF(handles);
guidata(hObject, handles);


function editNEF_vNoc_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function buttonNEF_loadEPNL_Callback(hObject, eventdata, handles)
EPNL=round(handles.EPNLData.EPNL,1);
set(handles.editNEF_EPNL,'String', num2str(EPNL))
handles=gui_calc_NEF(handles);
guidata(hObject, handles);

function buttonExtGraph_Callback(hObject, eventdata, handles)
plot_external(handles);


function buttonNEF_calc_Callback(hObject, eventdata, handles)
EPNL=str2double(get(handles.editNEF_EPNL,'String'));
vDia=str2double(get(handles.editNEF_vDia,'String'));
vNoc=str2double(get(handles.editNEF_vNoc,'String'));
vDia_A=str2double(get(handles.editNEF_vDia_A,'String'));
vNoc_A=str2double(get(handles.editNEF_vNoc_A,'String'));


if isempty(EPNL) || isnan(EPNL) || ...
        isempty(vDia) || isnan(vDia) || ...
        isempty(vNoc) || isnan(vNoc)
    errordlg('Debe especificar valores numericos para EPNL y cantidad de vuelos para calcular NEF.', 'Error')
    return
else
    handles=gui_calc_NEF(handles);
    guidata(hObject, handles);
end

if isempty(EPNL) || isnan(EPNL) || ...
        isempty(vDia_A) || isnan(vDia_A) || ...
        isempty(vNoc_A) || isnan(vNoc_A)
    errordlg('Debe especificar valores numericos para EPNL y cantidad de vuelos para calcular ANEF.', 'Error')
    return
else
    handles=gui_calc_NEF(handles);
    guidata(hObject, handles);
end

function handles = gui_calc_NEF(handles)
EPNL=str2double(get(handles.editNEF_EPNL,'String'));
vDia=str2double(get(handles.editNEF_vDia,'String'));
vNoc=str2double(get(handles.editNEF_vNoc,'String'));
vDia_A=str2double(get(handles.editNEF_vDia_A,'String'));
vNoc_A=str2double(get(handles.editNEF_vNoc_A,'String'));


if isempty(EPNL) || isnan(EPNL)
    handles.userParamsNEF.EPNL=zeros(0);
    return
elseif  ~isempty(vDia) && ~isnan(vDia) && ...
        ~isempty(vNoc) && ~isnan(vNoc)
    handles.userParamsNEF.EPNL=EPNL;
    handles.userParamsNEF.vDia=vDia;
    handles.userParamsNEF.vNoc=vNoc;
    
    NEF=calc_NEF(EPNL,vDia,vNoc);
    handles.NEFData.NEF=NEF;
    if isfield(handles.userParams,'avName')
        handles.NEFData.avName=handles.userParams.avName;
    end

    set(handles.editNEF_resultNEF,'String', strcat(num2str(round(NEF,0)),' NEF'))
    set(handles.mainFigure, 'NEFData', handles.NEFData)
    
elseif  isempty(vDia) || isnan(vDia) || ...
        isempty(vNoc) || isnan(vNoc)
    handles.userParamsNEF.EPNL=EPNL;
    handles.userParamsNEF.vDia=zeros(0);
    handles.userParamsNEF.vNoc=zeros(0);
    
    handles.NEFData.NEF=zeros(0);

    set(handles.editNEF_resultNEF,'String', '')
    set(handles.mainFigure, 'NEFData', handles.NEFData)
    
end

if isempty(EPNL) || isnan(EPNL)
    return
elseif  ~isempty(vDia_A) && ~isnan(vDia_A) && ...
        ~isempty(vNoc_A) && ~isnan(vNoc_A)
    handles.userParamsNEF.EPNL=EPNL;
    handles.userParamsNEF.vDia_A=vDia_A;
    handles.userParamsNEF.vNoc_A=vNoc_A;

    ANEF=calc_ANEF(EPNL,vDia_A,vNoc_A);
    handles.NEFData.ANEF=ANEF;

    if isfield(handles.userParams,'avName')
        handles.NEFData.avName=handles.userParams.avName;
    end
    set(handles.editNEF_resultANEF,'String', strcat(num2str(round(ANEF,0)),' ANEF'))
    set(handles.mainFigure, 'NEFData', handles.NEFData)

elseif  isempty(vDia_A) || isnan(vDia_A) || ...
        isempty(vNoc_A) || isnan(vNoc_A)
    handles.userParamsNEF.EPNL=EPNL;
    handles.userParamsNEF.vDia_A=zeros(0);
    handles.userParamsNEF.vNoc_A=zeros(0);
    
    handles.NEFData.ANEF=zeros(0);

    set(handles.editNEF_resultANEF,'String', '')
    set(handles.mainFigure, 'NEFData', handles.NEFData)
end

function menuArchivo_Callback(hObject, eventdata, handles)

function menuEntrada_Callback(hObject, eventdata, handles)


function menuE_loadCal_Callback(hObject, eventdata, handles)
calSPL=inputdlg('Nivel de calibración');

if length(calSPL)<1
    return
end

if ~isnan(str2double(calSPL{1,1})) && str2double(calSPL{1,1})>0
    handles = selectCalFile_Callback(hObject, eventdata, handles);
    handles.userParams.calSPL=str2double(calSPL{1,1});
    set(handles.editCalFileLevel,'String',calSPL{1,1})
    handles = buttonLoadCal_Callback(hObject, eventdata, handles);
    guidata(hObject,handles);
end






function menuE_calFactor_Callback(hObject, eventdata, handles)

calFactor=inputdlg('Factor de calibración');

if length(calFactor)<1
    return
end

if ~isnan(str2double(calFactor{1,1})) && str2double(calFactor{1,1})>0
    handles = selectCalFactor_Callback(hObject, eventdata, handles);
    handles.userParams.calFactor=str2double(calFactor{1,1});
    set(handles.editCalFactor,'String',calFactor{1,1})
    guidata(hObject,handles);
end


function menuE_loadAudio_Callback(hObject, eventdata, handles)
handles = buttonLoadCal_Callback(hObject, eventdata, handles);
guidata(hObject,handles);


function menuA_loadResult_Callback(hObject, eventdata, handles)
handles=load_results(handles);
guidata(hObject,handles);


function menuA_saveResult_Callback(hObject, eventdata, handles)
buttonSaveResult_Callback(hObject, eventdata, handles)

function menuA_exportResult_Callback(hObject, eventdata, handles)
buttonExport_Callback(hObject, eventdata, handles)



function menuA_exit_Callback(hObject, eventdata, handles)
close(handles.mainFigure)


function menuAyuda_Callback(hObject, eventdata, handles)


function menuAy_help_Callback(hObject, eventdata, handles)
gui_help()


function menuAy_about_Callback(hObject, eventdata, handles)
gui_about(handles.version)


function buttonNEFTotal_Callback(hObject, eventdata, handles)
gui_total_NEF(handles.mainFigure);



function editAvName_Callback(hObject, eventdata, handles)
avName=get(hObject,'String');

if ~isempty(avName)
    handles.userParams.avName=avName;
    handles.NEFData.avName=handles.userParams.avName;
    set(handles.mainFigure, 'NEFData', handles.NEFData)
end
guidata(hObject, handles)



function editAvName_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editNEF_vDia_A_Callback(hObject, eventdata, handles)
handles=gui_calc_NEF(handles);
guidata(hObject, handles);


function editNEF_vDia_A_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editNEF_vNoc_A_Callback(hObject, eventdata, handles)
handles=gui_calc_NEF(handles);
guidata(hObject, handles);


function editNEF_vNoc_A_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
