function [ filtros, filtrosStruct ] = generar_filtros(fs, fLim, tipo, orden)
%GENERAR_FILTROS Genera filtros pasabanda estandarizados
%   [ filtros, filtrosStruct ] = generar_filtros(fs, fLim, tipo, orden)
%   Entrada:
%   fs: frecuencia de muestreo de la señal a filtrar
%   fLim: vector de 2 elementos con la frecuencia minima y maxima que se
%   busca filtrar(0 a inf por defecto)
%   tipo: Divisiones por octava
%       Opciones: 1(octava, por defecto), 1/3(tercios de octava)
%   orden: orden del filtro(8 por defecto)
%
%   Salida
%   filtros: celda de elementos de filtro por cada banda
%   filtrosStruct: estructura con informacion de los filtros
%     filterStruct.filtros: celda de elementos de filtro por cada banda
%     filterStruct.fs: frecuencia de muestreo con la que se generaron los filtros
%     filterStruct.bandas: vector con las f0 de los filtros
%
%   Ejemplo:
%   filters = generar_filtros(44100, [50 10e3], 1/3) genera filtros por
%   tercio de octava entre 50 y 10kHz


if nargin < 2
   fLim=[0 Inf];
   tipo=1;    
   orden=8;
elseif nargin < 3
   tipo=1;    
   orden=8;
elseif nargin < 4
    orden=8;
end

%% Define bandas y tipo de filtro

switch tipo
    case 1
        bandasFilt.f0 = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
        bandasFilt.label=[31.5 63 125 250 500 1000 2000 4000 8000 16000];
        factor_IS=2^(1/2);
        
    case 1/3
        bandasFilt.f0 = [24.8 31.25 39.37 49.61 62.5 78.74 99.21 125 157.49 198.42 ...
            250 314.98 396.85 500 629.96 793.7 999.99 1259.91 1587.39 ...
            1999.99 2519.83 3174.78 3999.98 5039.66 6349.57 7999.95 10079.31 ...
            12699.14 15999.91 20158.62];
        bandasFilt.label= [25 31.5 40 50 63 80 100 125 160 200 250 315 400 ... 
            500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 6300 ...
            8000 10000 12500 16000 20000];
        factor_IS=2^(1/6);
end

%% Selecciono bandas que quiero utilizar
indexUse=bandasFilt.label >= fLim(1)&bandasFilt.label<=fLim(2);

bandasFilt.label = bandasFilt.label(indexUse);
bandas=bandasFilt.label;

bandasFilt.f0 = bandasFilt.f0(indexUse);
bandasFilt.fInf = bandasFilt.f0/factor_IS;
bandasFilt.fSup = bandasFilt.f0*factor_IS;

if fs/2>=22050 && fs/2<=max(bandasFilt.fSup)
    bandasFilt.fSup(bandasFilt.fSup>22050)=22050;
elseif fs/2<=max(bandasFilt.fSup)
    error('fs/2 es menor que la maxima frecuencia superior. Esto se transforma en aliasing.')
end


bandasNum=length(bandasFilt.f0);

%% Generacion filtros
filtros=cell(1,bandasNum);
for banda=1:bandasNum
    filtros{banda} = designfilt('bandpassiir','FilterOrder',orden,'HalfPowerFrequency1',bandasFilt.fInf(banda),'HalfPowerFrequency2',bandasFilt.fSup(banda),'SampleRate',fs);
end

filtrosStruct.filtros=filtros;
filtrosStruct.bandas=bandas;
filtrosStruct.fs=fs;



end

