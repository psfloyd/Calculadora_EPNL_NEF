function [ sumaEnergetica ] = suma_energetica( splMat, dim )
%SUMA_ENERGETICA Calcula la suma energetica de un vector en SPL. Normalmente utilizado para calcular valores globales en funcion de valores por banda.
%   [ sumaEnergetica ] = suma_energetica( splMat )
%   splMat: matriz de niveles de presion en SPL
%   dim: dimension en la cual se realiza la suma(para el caso de matrices)
%   sumaEnergetica: valor de la suma energetica

if nargin < 2
    dim=1;
    splSize=size(splMat);
    if splSize(1)==1
        dim=2;
    end
end

presMat = spl2pa(splMat);
presSuma = sqrt(sum(presMat.^(2),dim));

sumaEnergetica = pa2spl(presSuma);

end

