function [ NEF ] = calc_NEF( EPNL, vDia, vNoc )
%CALC_NEF Calcula el valor NEF en función de EPNL y cantidad de vuelos
%diurnos y nocturnos
%   [ NEF ] = calc_NEF( EPNL, vDia, vNoc )
%   EPNL: valor EPNL de la aeronave
%   vDia: cantidad de vuelos diurnos
%   vNoc: cantidad de vuelos nocturnos

NEF= EPNL + 10*log10( vDia + 17*vNoc) -88;
end

