function [ promedioEnergetico ] = promedio_energetico( presMat, dim )
%promedio_energetico Calcula el promedio energetico de un vector de presion
%   [ promedioEnergetico ] = promedio_energetico( presMat )
%   presMat: matriz de presiones en Pa
%   dim: dimension para la cual se realiza la sumatoria(para el caso de matrices)
%   promedioEnergetico: valor del promedio energetico en ese periodo

if nargin < 2
    dim=1;
    presSize=size(presMat);
    if presSize(1)==1
        dim=2;
    end
end


presLength=size(presMat,dim);
presSumatoria=sqrt(1/presLength*sum(presMat.^(2),dim));

promedioEnergetico = pa2spl( presSumatoria );


end

