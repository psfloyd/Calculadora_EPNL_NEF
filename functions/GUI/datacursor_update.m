function [ output_txt ] = datacursor_update(obj,event_obj)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).



window=obj.Parent.Parent.Parent;
targetPlot=get(obj.Parent, 'Tag');



pos = get(event_obj,'Position');
xPos=pos(1);

switch targetPlot
    case 'axesSpectrum'
        output_txt = {['f: ',num2str(window.EPNLData.bandasLabel{xPos})],...
            ['SPL: ',num2str(pos(2), 4)]};
    case 'axesTime'
        output_txt = {['t: ',num2str(xPos)],...
            ['SPL: ',num2str(pos(2),4)]};
    otherwise
        output_txt = {['X: ',num2str(pos(1),4)],...
            ['Y: ',num2str(pos(2),4)]};
        
        % If there is a Z-coordinate in the position, display it as well
        if length(pos) > 2
            output_txt{end+1} = ['Z: ',num2str(pos(3),4)];
        end
end



graphics_update(window, targetPlot, xPos);

end

