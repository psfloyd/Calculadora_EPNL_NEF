function varargout = gui_help(varargin)
% GUI_HELP MATLAB code for gui_help.fig
%      GUI_HELP, by itself, creates a new GUI_HELP or raises the existing
%      singleton*.
%
%      H = GUI_HELP returns the handle to a new GUI_HELP or the handle to
%      the existing singleton*.
%
%      GUI_HELP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_HELP.M with the given input arguments.
%
%      GUI_HELP('Property','Value',...) creates a new GUI_HELP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_help_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_help_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_help

% Last Modified by GUIDE v2.5 02-Aug-2018 22:55:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_help_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_help_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui_help is made visible.
function gui_help_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_help (see VARARGIN)

% Choose default command line output for gui_help
handles.output = hObject;
movegui(handles.help, 'center')
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui_help wait for user response (see UIRESUME)
% uiwait(handles.help);


% --- Outputs from this function are returned to the command line.
function varargout = gui_help_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
