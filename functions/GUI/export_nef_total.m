function export_nef_total( handles )
[filename,path]=uiputfile( {'*.csv','Archivo separado por comas(.csv)'}, 'Guardar Tabla Resultados:', 'Resultados.csv');

if ~isnumeric(filename)
    filepath=strcat(path,filename);
else
    return
end


% process Data
tableNEFTotal = NEFTotalProcess(handles);

writetable(tableNEFTotal, filepath,'Delimiter',';');

end

function [tableNEFTotal,cellNEFTotal] = NEFTotalProcess(handles)

NEFData=handles.NEFData;

indexName=1;
indexColDataNEF=2;
indexUnit=3;
cellNEFTotal=cell(0);

cellNEFTotal=[NEFData.nombres' NEFData.valoresNEF' NEFData.valoresANEF'];
cellNEFTotal(end+1,:) = {'Total' NEFData.NEFTotal NEFData.ANEFTotal};

tableNEFTotal = cell2table(cellNEFTotal, 'VariableNames',{'Nombre' 'NEF' 'ANEF'} );

end