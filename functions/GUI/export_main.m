function export_main( handles )
[filename,path]=uiputfile( {'*.csv','Archivo separado por comas(.csv)'}, 'Guardar Tabla Resultados:', 'Resultados.csv');

if ~isnumeric(filename)
    filepath=strcat(path,filename);
else
    return
end


% process Data
EPNLData=handles.EPNLData;
userParams=handles.userParams;
NEFData=handles.NEFData;
userParamsNEF=handles.userParamsNEF;


tableEPNL = EPNLprocess(EPNLData, userParams);

tableNEF = NEFprocess(NEFData, userParamsNEF);

tableExport=[tableEPNL;tableNEF];

writetable(tableExport, filepath,'Delimiter',';');


end

%% EPNLprocess{{{
function [ tableEPNL, cellEPNL ] = EPNLprocess(EPNLData,userParams)

indexName=1;
indexColData=2;
indexUnit=3;
cellEPNL=cell(0);

cellEPNL{1,indexName}='Nombre de la medicion';
cellEPNL{2,indexName}='Nombre de archivo';
cellEPNL{3,indexName}='Factor de calibracion';
cellEPNL{4,indexName}='Archivo de calibracion';
cellEPNL{5,indexName}='Nivel de calibracion';
cellEPNL{6,indexName}='Max. nivel de presion';
cellEPNL{7,indexName}='Max. nivel corregido por ruidosidad';
cellEPNL{8,indexName}='Max. nivel corregido por riudosidad y componentes tonales';
cellEPNL{9,indexName}='EPNL';

cellEPNL{1,indexUnit}='-';
cellEPNL{2,indexUnit}='-';
cellEPNL{3,indexUnit}='-';
cellEPNL{4,indexUnit}='-';
cellEPNL{5,indexUnit}='[dB SPL]';
cellEPNL{6,indexUnit}='[dB SPL]';
cellEPNL{7,indexUnit}='[dB SPL]';
cellEPNL{8,indexUnit}='[dB SPL]';
cellEPNL{9,indexUnit}='[EPNdB]';

% nombre de la medicion
cellEPNL{1,indexColData}=userParams.avName;

% nombre de archivo
cellEPNL{2,indexColData}=extFilename(userParams.audioFilepath);


if strcmp(userParams.calMode,'factor')
% factor de calibracion
    cellEPNL{3,indexColData}=round(userParams.calFactor,1);
% archivo de calibracion

cellEPNL{4,indexColData}='-';
% nivel de calibracion
cellEPNL{5,indexColData}='-';

elseif strcmp(userParams.calMode,'file')
% factor de calibracion
    cellEPNL{3,indexColData}='-';
% archivo de calibracion
cellEPNL{4,indexColData}=extFilename(userParams.calFilepath);
% nivel de calibracion
cellEPNL{5,indexColData}=round(userParams.calSPL,1);
end
% Lp max
% L_PN max
% L_PNT max
% EPNL


cellEPNL{6,indexColData}=round(max(EPNLData.L_P),1);
cellEPNL{7,indexColData}=round(max(EPNLData.L_PN),1);
cellEPNL{8,indexColData}=round(max(EPNLData.L_PNT),1);
cellEPNL{9,indexColData}=round(EPNLData.EPNL,1);


% conversion a table

tableEPNL = cell2table(cellEPNL, 'VariableNames',{'Categoria' 'Valor' 'Unidad'} );

end
%%}}}

% NEFprocess{{{
function [ tableNEF, cellNEF ] = NEFprocess(NEFData, userParamsNEF)
indexName=1;
indexColData=2;
indexUnit=3;
cellNEF=cell(0);


cellNEF{1,indexName}='Calculo de NEF';
cellNEF{2,indexName}='Nombre de la medicion';
cellNEF{3,indexName}='EPNL';
cellNEF{4,indexName}='Cant. vuelos Dia(7-22hs)';
cellNEF{5,indexName}='Cant. vuelos Noche(22-7hs)';
cellNEF{6,indexName}='NEF';
cellNEF{7,indexName}='Cant. vuelos Dia(7-19hs)';
cellNEF{8,indexName}='Cant. vuelos Noche(19-7hs)';
cellNEF{9,indexName}='ANEF';

cellNEF{1,indexUnit}='';
cellNEF{2,indexUnit}='-';
cellNEF{3,indexUnit}='[EPNdB]';
cellNEF{4,indexUnit}='-';
cellNEF{5,indexUnit}='-';
cellNEF{6,indexUnit}='[NEF]';
cellNEF{7,indexUnit}='-';
cellNEF{8,indexUnit}='-';
cellNEF{9,indexUnit}='[ANEF]';

% Nombre
% EPNL
% Cant Vuelos dia
% Cant Vuelos noche
% NEF
% Cant Vuelos dia(A)
% Cant Vuelos noche(A)
% ANEF
cellNEF{1,indexColData}='';
cellNEF{2,indexColData}=NEFData.avName;
cellNEF{3,indexColData}=round(userParamsNEF.EPNL,1);
cellNEF{4,indexColData}=round(userParamsNEF.vDia,0);
cellNEF{5,indexColData}=round(userParamsNEF.vNoc,0);
cellNEF{6,indexColData}=round(NEFData.NEF,0);
cellNEF{7,indexColData}=round(userParamsNEF.vDia_A,0);
cellNEF{8,indexColData}=round(userParamsNEF.vNoc_A,0);
cellNEF{9,indexColData}=round(NEFData.ANEF,0);


% conversion a table

tableNEF = cell2table(cellNEF, 'VariableNames',{'Categoria' 'Valor' 'Unidad'} );

end
%}}}

function name = extFilename(filepath)
    [~,name,~] = fileparts(filepath);
end
