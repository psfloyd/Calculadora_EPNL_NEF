function [  ] = graphics_update( window, targetPlot, xPos )

EPNLData=window.EPNLData;
handles.EPNLData=EPNLData;

% indexPanelResult=contains(namem(window.Children), 'panelResult');
% indexAxesTime=contains(namem(window.Children(indexPanelResult).Children), 'axesTime');
% indexAxesSpectrum=contains(namem(window.Children(indexPanelResult).Children), 'axesSpectrum');
% 
% handles.axesSpectrum=window.Children(indexPanelResult).Children(indexAxesSpectrum);
% handles.axesTime=window.Children(indexPanelResult).Children(indexAxesTime);


handles.axesSpectrum=findobj(window,'Tag','axesSpectrum', 'Type', 'Axes');
handles.axesTime=findobj(window,'Tag','axesTime', 'Type', 'Axes');
switch targetPlot
    case  'axesTime'
        
        if xPos>=0
            [~,indexSpectrum]=max(EPNLData.t==xPos);
            
            bar(handles.axesSpectrum, EPNLData.spectrMat(:,indexSpectrum+1))
            legend(handles.axesSpectrum, char(strcat('t: ',num2str(xPos),'s')))
            
        else
            bar(handles.axesSpectrum, EPNLData.spectrMat(:,1))
            legend(handles.axesSpectrum, 'Total')
        end
        set_plot_options(handles)
        
        
    case 'axesSpectrum'
        
        plot(handles.axesTime, EPNLData.t, EPNLData.timeMat(xPos,:), EPNLData.t, EPNLData.L_PN, EPNLData.t, EPNLData.L_PNT)
        
        if xPos>1
            legend(handles.axesTime, char(strcat('Sin corr. (',EPNLData.bandasLabel(xPos),'Hz)')),'Corr. por ruidosidad', 'Corr. por tonos')
        else
            legend(handles.axesTime, char(strcat('Sin corr. (',EPNLData.bandasLabel(xPos),')')),'Corr. por ruidosidad', 'Corr. por tonos')
        end
        
        set_plot_options(handles)
        
end

end


function set_plot_options(handles)

% axesTime
set(handles.axesTime,'Tag','axesTime')

xlabel(handles.axesTime, 'Tiempo[s]')
ylabel(handles.axesTime, 'Nivel[dB SPL]')


% axesSpectrum
set(handles.axesSpectrum,'Tag','axesSpectrum')
xlabel(handles.axesSpectrum, 'Frecuencia[Hz]')
ylabel(handles.axesSpectrum, 'Nivel[dB SPL]')
bandasPlotLabel={'' handles.EPNLData.bandasLabel{:} };
xlim(handles.axesSpectrum, [0 length(bandasPlotLabel)])
% xticks(handles.axesSpectrum, 0:1:length(bandasPlotLabel))
% xticklabels(handles.axesSpectrum, bandasPlotLabel)
% xtickangle(handles.axesSpectrum, 45)

set(handles.axesSpectrum, 'XTick',0:1:length(bandasPlotLabel))
set(handles.axesSpectrum, 'XTickLabel', bandasPlotLabel)
set(handles.axesSpectrum, 'XTickLabelRotation', 45)

end

