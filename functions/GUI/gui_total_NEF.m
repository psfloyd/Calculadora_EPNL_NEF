function varargout = gui_total_NEF(varargin)
% GUI_TOTAL_NEF MATLAB code for gui_total_NEF.fig
%      GUI_TOTAL_NEF, by itself, creates a new GUI_TOTAL_NEF or raises the existing
%      singleton*.
%
%      H = GUI_TOTAL_NEF returns the handle to a new GUI_TOTAL_NEF or the handle to
%      the existing singleton*.
%
%      GUI_TOTAL_NEF('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_TOTAL_NEF.M with the given input arguments.
%
%      GUI_TOTAL_NEF('Property','Value',...) creates a new GUI_TOTAL_NEF or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_total_NEF_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_total_NEF_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_total_NEF

% Last Modified by GUIDE v2.5 16-Sep-2018 12:07:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_total_NEF_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_total_NEF_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui_total_NEF is made visible.
function gui_total_NEF_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_total_NEF (see VARARGIN)

% Choose default command line output for gui_total_NEF
handles.output = hObject;

handles.uInternal.mainFigure=varargin{1};

%set(handles.totalNEFCalc,'WindowStyle','modal')

handles.obj.editNEF{1} =handles.editNEF_01;
handles.obj.editNEF{2} =handles.editNEF_02;
handles.obj.editNEF{3} =handles.editNEF_03;
handles.obj.editNEF{4} =handles.editNEF_04;
handles.obj.editNEF{5} =handles.editNEF_05;
handles.obj.editNEF{6} =handles.editNEF_06;
handles.obj.editNEF{7} =handles.editNEF_07;
handles.obj.editNEF{8} =handles.editNEF_08;
handles.obj.editNEF{9} =handles.editNEF_09;
handles.obj.editNEF{10}=handles.editNEF_10;
handles.obj.editNEF{11}=handles.editNEF_11;
handles.obj.editNEF{12}=handles.editNEF_12;
handles.obj.editNEF{13}=handles.editNEF_13;
handles.obj.editNEF{14}=handles.editNEF_14;
handles.obj.editNEF{15}=handles.editNEF_15;
handles.obj.editNEF{16}=handles.editNEF_16;

handles.obj.editANEF{1} =handles.editANEF_01;
handles.obj.editANEF{2} =handles.editANEF_02;
handles.obj.editANEF{3} =handles.editANEF_03;
handles.obj.editANEF{4} =handles.editANEF_04;
handles.obj.editANEF{5} =handles.editANEF_05;
handles.obj.editANEF{6} =handles.editANEF_06;
handles.obj.editANEF{7} =handles.editANEF_07;
handles.obj.editANEF{8} =handles.editANEF_08;
handles.obj.editANEF{9} =handles.editANEF_09;
handles.obj.editANEF{10}=handles.editANEF_10;
handles.obj.editANEF{11}=handles.editANEF_11;
handles.obj.editANEF{12}=handles.editANEF_12;
handles.obj.editANEF{13}=handles.editANEF_13;
handles.obj.editANEF{14}=handles.editANEF_14;
handles.obj.editANEF{15}=handles.editANEF_15;
handles.obj.editANEF{16}=handles.editANEF_16;

handles.obj.btPlus{1} =handles.btPlus_01;
handles.obj.btPlus{2} =handles.btPlus_02;
handles.obj.btPlus{3} =handles.btPlus_03;
handles.obj.btPlus{4} =handles.btPlus_04;
handles.obj.btPlus{5} =handles.btPlus_05;
handles.obj.btPlus{6} =handles.btPlus_06;
handles.obj.btPlus{7} =handles.btPlus_07;
handles.obj.btPlus{8} =handles.btPlus_08;
handles.obj.btPlus{9} =handles.btPlus_09;
handles.obj.btPlus{10}=handles.btPlus_10;
handles.obj.btPlus{11}=handles.btPlus_11;
handles.obj.btPlus{12}=handles.btPlus_12;
handles.obj.btPlus{13}=handles.btPlus_13;
handles.obj.btPlus{14}=handles.btPlus_14;
handles.obj.btPlus{15}=handles.btPlus_15;
handles.obj.btPlus{16}=handles.btPlus_16;

handles.obj.textAv{1} =handles.textAv_01;
handles.obj.textAv{2} =handles.textAv_02;
handles.obj.textAv{3} =handles.textAv_03;
handles.obj.textAv{4} =handles.textAv_04;
handles.obj.textAv{5} =handles.textAv_05;
handles.obj.textAv{6} =handles.textAv_06;
handles.obj.textAv{7} =handles.textAv_07;
handles.obj.textAv{8} =handles.textAv_08;
handles.obj.textAv{9} =handles.textAv_09;
handles.obj.textAv{10}=handles.textAv_10;
handles.obj.textAv{11}=handles.textAv_11;
handles.obj.textAv{12}=handles.textAv_12;
handles.obj.textAv{13}=handles.textAv_13;
handles.obj.textAv{14}=handles.textAv_14;
handles.obj.textAv{15}=handles.textAv_15;
handles.obj.textAv{16}=handles.textAv_16;

for k=1:length(handles.obj.editNEF)
    set(handles.obj.editNEF{k}, 'UserData', {k 'NEF'}) 
    set(handles.obj.editANEF{k}, 'UserData', {k 'ANEF'}) 
    set(handles.obj.textAv{k}, 'UserData', {k})
    set(handles.obj.btPlus{k}, 'UserData', {k})
    %handles.NEFData.nombres{k} = ['Aeronave ' num2str(k)];
end

handles.NEFData.nombres=cell(0);
handles.NEFData.valoresNEF=cell(0);
handles.NEFData.valoresANEF=cell(0);

handles.uInternal.addSym='<';
handles.uInternal.remSym='-';

set(handles.obj.btPlus{1}, 'String', handles.uInternal.addSym)

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui_total_NEF wait for user response (see UIRESUME)
% uiwait(handles.totalNEFCalc);


% --- Outputs from this function are returned to the command line.
function varargout = gui_total_NEF_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function handles = button_plus(hObject,handles)
% determinar que linea es
tmpUserData=get(hObject, 'UserData');
linea=tmpUserData{1};

% cargar valor de nef y nombre de la gui
NEFData=get(handles.uInternal.mainFigure,'NEFData');
if isempty(NEFData)
    return
end
NEF=round(NEFData.NEF,2);
ANEF=round(NEFData.ANEF,2);
handles.NEFData.valoresNEF{linea}=NEF;
handles.NEFData.valoresANEF{linea}=ANEF;
if isfield(NEFData, 'avName')
    handles.NEFData.nombres{linea}=NEFData.avName;
else
    currName={get(handles.obj.textAv{linea},'String')};
    newName=inputdlg('Nombre de la aeronave:','Nombre',1,currName);
    if ~isempty(newName)
        handles.NEFData.nombres{linea}=newName{1};
    else
        handles.NEFData.nombres{linea}=currName{1};
    end
end


% Editar visibilidad de elementos

handles=update_GUI(handles);

cantElementos=length(handles.obj.editNEF);
if linea<cantElementos
    set(handles.obj.btPlus{linea+1} ,'Visible','on','String',handles.uInternal.addSym)
    set(handles.obj.editNEF{linea+1},'Visible','on')
    set(handles.obj.editANEF{linea+1},'Visible','on')
end


function handles = button_minus(hObject, handles)
% determinar que linea es
tmpUserData=get(hObject, 'UserData');
linea=tmpUserData{1};

% eliminar el valor y nombre de la lista
cantData=length(handles.NEFData.valoresNEF);
if linea>1 && linea<cantData
    handles.NEFData.valoresNEF=[ handles.NEFData.valoresNEF(1:linea-1) handles.NEFData.valoresNEF(linea+1:end) ];
    handles.NEFData.valoresANEF=[ handles.NEFData.valoresANEF(1:linea-1) handles.NEFData.valoresANEF(linea+1:end) ];
    handles.NEFData.nombres   =[ handles.NEFData.nombres(1:linea-1)    handles.NEFData.nombres(linea+1:end) ];
elseif linea==1
    handles.NEFData.valoresNEF=[ handles.NEFData.valoresNEF(linea+1:end) ];
    handles.NEFData.valoresANEF=[ handles.NEFData.valoresANEF(linea+1:end) ];
    handles.NEFData.nombres   =[ handles.NEFData.nombres(linea+1:end) ];
else
    handles.NEFData.valoresNEF=[ handles.NEFData.valoresNEF(1:end-1) ];
    handles.NEFData.valoresANEF=[ handles.NEFData.valoresANEF(1:end-1) ];
    handles.NEFData.nombres   =[ handles.NEFData.nombres(1:end-1) ];
end

% Editar visibilidad de elementos
handles=update_GUI(handles);

cantData=length(handles.NEFData.valoresNEF);
cantElementos=length(handles.obj.editNEF);

for k2=cantData+1:cantElementos
    set(handles.obj.editNEF{k2},'Visible','off','String','')
    set(handles.obj.editANEF{k2},'Visible','off','String','')
    set(handles.obj.btPlus{k2} ,'Visible','off')
    set(handles.obj.textAv{k2}   ,'Visible','off','String','')
end

set(handles.obj.editNEF{cantData+1},'Visible','on')
set(handles.obj.editANEF{cantData+1},'Visible','on')
set(handles.obj.btPlus{cantData+1} ,'Visible','on','String',handles.uInternal.addSym)




function handles = edit_text(hObject, handles)
% determinar que linea es
tmpUserData=get(hObject, 'UserData');
tipo=tmpUserData{2};
linea=tmpUserData{1};

switch tipo
    case 'NEF'
        valorNEF=str2num(get(hObject, 'String'));
        if isempty(valorNEF) || isnan(valorNEF)
            errordlg('El valor de NEF debe ser numerico.','Error')
            set(hObject,'String','')
            return
        end
        handles.NEFData.valoresNEF{linea}=round(valorNEF,2);
    case 'ANEF'
        valorANEF=str2num(get(hObject, 'String'));
        if isempty(valorANEF) || isnan(valorANEF)
            errordlg('El valor de ANEF debe ser numerico.','Error')
            handles.NEFData.valoresANEF{linea}='';
            set(hObject,'String','')
            handles=update_GUI(handles);
            return
        end
        handles.NEFData.valoresANEF{linea}=round(valorANEF,2);
end

if linea>length(handles.NEFData.nombres)
    currName={['Aeronave ' num2str(linea)]};
    newName=inputdlg('Nombre de la aeronave:','Nombre',1,currName);
    if ~isempty(newName)
        handles.NEFData.nombres{linea}=newName{1};
    else
        handles.NEFData.nombres{linea}=currName{1};
    end
end

handles=update_GUI(handles);

% if isempty(get(handles.obj.editNEF{linea},'String')) && isempty(get(handles.obj.editANEF{linea},'String'))
%     handles = button_minus(handles.obj.btPlus{linea}, handles);
% end

cantElementos=length(handles.obj.editNEF);
if linea<cantElementos
    set(handles.obj.btPlus{linea+1} ,'Visible','on','String',handles.uInternal.addSym)
    set(handles.obj.editNEF{linea+1},'Visible','on')
    set(handles.obj.editANEF{linea+1},'Visible','on')
end


function handles=update_GUI(handles)
% NEF
cantDataNEF=length(handles.NEFData.valoresNEF);
NEFmat=zeros(0);
for k=1:cantDataNEF
    set(handles.obj.editNEF{k},'Visible','on')
    set(handles.obj.btPlus{k},'Visible','on')
    set(handles.obj.textAv{k},'Visible','on')
    set(handles.obj.btPlus{k},'String',handles.uInternal.remSym)
    set(handles.obj.textAv{k},'String',handles.NEFData.nombres{k})
    set(handles.obj.editNEF{k},'String',round(handles.NEFData.valoresNEF{k},0))
    
    if ~isempty(handles.NEFData.valoresNEF{k})
        NEFmat(end+1)=handles.NEFData.valoresNEF{k};
    end
end

NEFTotal=round(suma_energetica(NEFmat),2);
if ~isempty(NEFTotal)
    if NEFTotal>-Inf
        set(handles.editNEF_Total,'String', char(strcat(num2str(round(NEFTotal,0)), ' dB')))
        handles.NEFData.NEFTotal=NEFTotal;
    else
        set(handles.editNEF_Total,'String', '')
        handles.NEFData.NEFTotal='';
    end
else
    set(handles.editNEF_Total,'String', '')
    handles.NEFData.NEFTotal='';
end

% ANEF
cantDataANEF=length(handles.NEFData.valoresANEF);
ANEFmat=zeros(0);
for k=1:cantDataANEF
    set(handles.obj.editANEF{k},'Visible','on')
    set(handles.obj.editANEF{k},'String',round(handles.NEFData.valoresANEF{k},0))
    if ~isempty(handles.NEFData.valoresANEF{k})
        ANEFmat(end+1)=handles.NEFData.valoresANEF{k};
    end
end

ANEFTotal=round(suma_energetica(ANEFmat),2);
if ~isempty(ANEFTotal)
    if ANEFTotal>-Inf
        set(handles.editANEF_Total,'String', char(strcat(num2str(round(ANEFTotal,0)), ' dB')))
        handles.NEFData.ANEFTotal=ANEFTotal;
    else
        set(handles.editANEF_Total,'String', '')
        handles.NEFData.ANEFTotal='';
    end
else
    set(handles.editANEF_Total,'String', '')
    handles.NEFData.ANEFTotal='';
end




%function handles = names_text(hObject, handles)
%tmpUserData=get(hObject, 'UserData');
%linea=tmpUserData{1};

%currName={get(handles.obj.textAv{linea},'String')};
%newName=inputdlg('Nombre de la aeronave:','Nombre',1,currName);
%if ~isempty(newName)
    %handles.NEFData.nombres{linea}=newName{1};
%else
    %handles.NEFData.nombres{linea}=currName{1};
%end


%% save and load
function btSave_Callback(hObject, eventdata, handles)
[filename,path]=uiputfile( {'*.mat','Archivos MAT (*.mat)'}, 'Guardar Resultados:', 'NEFTotal.mat');

if ~isnumeric(filename)
    filepath=strcat(path,filename);
    NEFData=handles.NEFData;
    
    save(filepath, 'NEFData')
end


function btLoad_Callback(hObject, eventdata, handles)
handles=load_results(handles);

cantLineas=length(handles.NEFData.nombres);
cantElementos=length(handles.obj.editNEF);
if cantLineas<cantElementos
    set(handles.obj.btPlus{cantLineas+1} ,'Visible','on','String',handles.uInternal.addSym)
    set(handles.obj.editNEF{cantLineas+1},'Visible','on')
    set(handles.obj.editANEF{cantLineas+1},'Visible','on')
end

guidata(hObject, handles);


function handles = load_results(handles)

[filename,path]=uigetfile( {'*.mat','Archivos MAT (*.mat)'}, 'Cargar Resultados:', 'NEFTotal.mat');

if ~isnumeric(filename)
    filepath=strcat(path,filename);
    tmpLoad=load(filepath);
    
    if ~isfield(tmpLoad,'NEFData')
        if ~isfield(tmpLoad.NEFData,'valoresNEF')
            errordlg('Archivo no valido', 'Error')
            return
        end
    end
    
    handles.NEFData=tmpLoad.NEFData;
    
    handles=update_GUI(handles);
end

function btExport_Callback(hObject, eventdata, handles)
export_nef_total(handles)

%% edit text boxs
% NEF
function editNEF_01_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_01_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_02_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_02_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_03_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_03_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_04_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_04_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_05_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_05_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_06_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_06_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_07_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_07_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_08_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_08_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_09_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_09_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_10_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_10_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_11_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_11_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_12_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_12_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_13_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_13_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_14_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_14_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_15_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_15_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editNEF_16_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editNEF_16_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);

% ANEF
function editANEF_01_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_01_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_02_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_02_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_03_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_03_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_04_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_04_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_05_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_05_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_06_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_06_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_07_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_07_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_08_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_08_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_09_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_09_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_10_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_10_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_11_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_11_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_12_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_12_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_13_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_13_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_14_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_14_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_15_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_15_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


function editANEF_16_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function editANEF_16_Callback(hObject, eventdata, handles)
handles = edit_text(hObject, handles);
guidata(hObject,handles);


%% button plus
function btPlus_01_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_02_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_03_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_04_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_05_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_06_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_07_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_08_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_09_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_10_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_11_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_12_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_13_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_14_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_15_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);


function btPlus_16_Callback(hObject, eventdata, handles)
switch get(hObject,'String')
case handles.uInternal.addSym
    handles = button_plus(hObject,handles);
case handles.uInternal.remSym
    handles = button_minus(hObject,handles);
end
guidata(hObject, handles);
