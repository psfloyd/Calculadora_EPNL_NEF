function [ ANEF ] = calc_ANEF( EPNL, vDia, vNoc )
%CALC_ANEF Calcula el valor ANEF en función de EPNL y cantidad de vuelos
%diurnos y nocturnos
%   [ ANEF ] = calc_ANEF( EPNL, vDia, vNoc )
%   EPNL: valor EPNL de la aeronave
%   vDia: cantidad de vuelos diurnos
%   vNoc: cantidad de vuelos nocturnos

ANEF= EPNL + 10*log10( vDia + 4*vNoc) -88;
end

