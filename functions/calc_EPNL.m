function [ EPNL, output, filtrosStrcut ] = calc_EPNL( audioName, calFactor, filtrosStrcut )
%CALC_EPNL Corrige una señal de audio de un despegue o aterrizaje de avion
%de acuerdo con la norma de la FAA
% [ EPNL, output, filtrosStrcut ] = calc_EPNL( audioName, calFactor, filtrosStrcut )
%   audioName: ruta del archivo de audio a analizar
%   calFactor: factor de calibracion
%   filtrosStruct: filtros que se utilizan(para optimizar tiempos y no regenrar)
%   EPNL: valor de EPNL obtenido
%   output: estructura con valores intermedios de calculo
%       output.t: vector de tiempo de analisis(intervalos de 0.5s)
%       output.L_P: nivel de presion sin correccion
%       output.SPL(i,j): Matriz de i bandas con j puntos de tiempo
%       output.L_PN: nivel con correccion por ruidosidad
%       output.Ck: factor Ck para correccion tonal
%       output.L_PNT: nivel con correccion por ruidosidad y tonos
%       output.L_PNTM: nivel maximo obtenido en correccion por ruidosidad y tonos
%       output.D: factor D para obtener EPNL
%       output.EPNL

% definir si tengo filtros como entrada
generateFilters=nargin<3;



%% cargar audio

[audioVector,fs]=audioread(audioName); audioVector=audioVector(:,1); % aseguro que sea mono
if size(audioVector,1)>size(audioVector,2); audioVector=audioVector';end
audioLength = length(audioVector);

if generateFilters
    % generar filtros
    [~, filtrosStrcut] = generar_filtros(fs, [50 10e3], 1/3);
elseif filtrosStrcut.fs~=fs
    [~, filtrosStrcut] = generar_filtros(fs, [50 10e3], 1/3);
end
filtros=filtrosStrcut.filtros;
bandas=filtrosStrcut.bandas;
cantBandas=length(bandas);

% calibro
presVector=calFactor.*audioVector;

Dt=.5;
samplesK=Dt*fs;
cantK=floor(audioLength/samplesK);

L_P=zeros(1,cantK);
for k=1:cantK
    L_P(k)= promedio_energetico(presVector((k-1)*samplesK+1:k*samplesK),2);
end



%% filtro por tercios

% aplico filtros y genero matriz
presMatrix = filtrar_audio(presVector, filtros);

%% divicion en periodos de .5s


SPL=zeros(cantBandas, cantK);
for k=1:cantK
    SPL(:,k) = promedio_energetico(presMatrix(:,(k-1)*samplesK+1:k*samplesK),2);
end

%% calculo de valores Noy

noys=spl2noys(SPL);

%% Calculo N(k)

noisiness=zeros(1,cantK);
for k=1:cantK
    noisiness(k)= 0.85*max(noys(:,k)) + 0.15*sum(noys(:,k));
end

%% Calculo PNL corregida por ruidosidad

L_PN= 40 + 10*log10(noisiness)/(log10(2));

%% Correccion por C(k)

Ck=calc_Ck(SPL);

%% Calculo PNLT y PNLTM

L_PNT=L_PN+Ck;
[L_PNTM, indPNLTM]=max(L_PNT);

if Ck(indPNLTM)<mean(Ck(indPNLTM+1:indPNLTM+5))
    L_PNTM=L_PN(indPNLTM)+mean(Ck(indPNLTM+1:indPNLTM+5));
end

%% Correccion de duracion

tStart=find(L_PNT>L_PNTM-10,1);

for k=tStart:cantK
    tEnd=k;
    if all(L_PNT(k:end)<L_PNTM-10)
        break
    end
end

% constante de tiempo segun FAA
T=10;

D=10*log10( 1/T*sum(Dt * 10.^(L_PNT(tStart:tEnd-1)/10)) ) - L_PNTM;

%% Calculo de EPNL

EPNL= L_PNTM+D;

%% Variable para exportar

output.t = 0:.5:.5*(cantK-1);
output.L_P = L_P;
output.SPL = SPL;
output.L_PN = L_PN;
output.Ck = Ck;
output.L_PNT = L_PNT;
output.L_PNTM = L_PNTM;
output.D = D;
output.EPNL = EPNL;

end
