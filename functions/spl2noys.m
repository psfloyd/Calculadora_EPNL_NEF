function [ noys ] = spl2noys( SPL )
%SPL2NOYS Convierte valores de SPL a noys
%   [ noys ] = spl2noys( SPLMatrix )
%   SPL(i,k): matriz de SPL para k intervalos de tiempo y i=24 bandas
%   de tercios entre 50 y 10kHz

tabla_noys; % cargo la tabla de conversion a noys

[bandasNum,audioLength]=size(SPL);

noys=zeros(bandasNum,audioLength);


%% Conversion a noys

for banda=1:bandasNum
    % indices para cada caso
    indE=SPL(banda,:)<tablanoys.SPLd(banda);
    
    indD=SPL(banda,:)>=tablanoys.SPLd(banda) & SPL(banda,:)<tablanoys.SPLe(banda);
    
    indC=SPL(banda,:)>=tablanoys.SPLe(banda) & SPL(banda,:)<tablanoys.SPLb(banda);
    
    indB=SPL(banda,:)>=tablanoys.SPLb(banda) & SPL(banda,:)<tablanoys.SPLa(banda);
    
    indA=SPL(banda,:)>=tablanoys.SPLa(banda);
    
    % Noys para cada caso
    noys(banda,indE) = 0.1;
    
    noys(banda,indD) = 0.1*10.^(tablanoys.Md(banda).*(SPL(banda,indD) - tablanoys.SPLd(banda)));
    
    noys(banda,indC) = 0.3*10.^(tablanoys.Me(banda).*(SPL(banda,indC) - tablanoys.SPLe(banda)));
    
    noys(banda,indB) =     10.^(tablanoys.Mb(banda).*(SPL(banda,indB) - tablanoys.SPLb(banda)));
    
    noys(banda,indA) =     10.^(tablanoys.Mc(banda).*(SPL(banda,indA) - tablanoys.SPLc(banda)));
    
end

end

