function [ splMat ] = pa2spl( presionMat )
%PA2SPL Convierte valores de presion[Pa] en dB SPL
%   [ spl ] = pa2spl( presion )
%   presion: matriz con valores de presion en Pa
%   spl: matriz con niveles de presion en SPL

splMat = 10.*log10(presionMat.^(2)./(20e-6.^2));


end

