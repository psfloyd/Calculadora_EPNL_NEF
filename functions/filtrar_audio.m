function [ presMatrix ] = filtrar_audio( presVector, filters )
%FILTRAR_AUDIO Summary of this function goes here
%   Detailed explanation goes here

bandasNum=size(filters, 2);

audioLength=length(presVector);

presMatrix=zeros(bandasNum, audioLength);

% Filtrado del audio y armado de la matriz
for banda = 1:bandasNum
    presMatrix(banda,:) = filtfilt(filters{banda}, presVector);
end

end

