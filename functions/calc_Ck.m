function [ Ck ] = calc_Ck( SPL )
%CALC_CK Calcula el factor C(k) para correccion tonal
%   [ Ck ] = calc_Ck( SPL )
%   SPL(i,k): matriz de SPL para k intervalos de tiempo y i=24 bandas
%   de tercios entre 50 y 10kHz

%% definicion de dimensiones
[cantBandas,cantK]=size(SPL);
if cantBandas~=24; error('SPL matriz de forma incorrecta');end
bandas=[50 63 80 100 125 160 200 250 315 400 500 630 800 1000 1250 1600 ...
        2000 2500 3150 4000 5000 6300 8000 10000];

%% calculo s

s=zeros(cantBandas,cantK);

s(4:end,:)=SPL(4:end,:)-SPL(3:end-1,:);

%% se buscan valores que hay que resaltar de s

resaS=false(cantBandas,cantK);

for k = 1:cantK
    for  i=2:cantBandas
        if abs(s(i,k)-s(i-1,k))>5
            resaS(i,k)=true;
        end
        
    end
end

%% valores que hay que resaltar de SPL

resaSPL=false(cantBandas,cantK);
for k = 1:cantK
    for  i=2:cantBandas
        if resaS(i,k)
            if s(i,k)>0 && s(i,k)>s(i-1,k)
                resaSPL(i,k)=true;
                
            elseif s(i,k)<=0 && s(i-1,k)>0
                resaSPL(i-1,k)=true;
            end
        end
    end
end


%% correccion de los valores SPL


SPLprima=SPL;
for k = 1:cantK
    for i = 2:cantBandas
        if i<24 && resaSPL(i,k)
            SPLprima(i,k)=(SPL(i-1,k)+SPL(i+1,k)) / 2;
            
        elseif resaSPL(i,k)
            SPLprima(i,k)=SPL(i-1,k)+s(i-1,k);
        end
    end
end



%% calculo s'

sPrima=zeros(cantBandas+1,cantK);

sPrima(4:end-1,:) = SPLprima(4:end,:) - SPLprima(3:end-1,:);
sPrima(3,:)=sPrima(4,:);
sPrima(end,:)=sPrima(end-1,:);

sBarra=zeros(cantBandas,cantK);

sBarra(3:23,:)=1/3*(sPrima(3:23,:)+sPrima(4:24,:)+sPrima(5:25,:));

%% calculo SPL''

SPL2prima=zeros(cantBandas,cantK);

SPL2prima(3,:)=SPL(3,:);
SPL2prima(4:end,:)=SPL(3:end-1,:) + sBarra(3:end-1,:);

%% se calcula F(i,k)

F=SPL-SPL2prima;

%% determinacion de C(i,k) (correccion tonal)

Cik=zeros(cantBandas,cantK);

for k = 1:cantK
    for i = 3:cantBandas
        if     bandas(i)>=50 && bandas(i)<500
            if F(i,k)>=1.5 && F(i,k)<3
                Cik(i,k)=F(i,k)/3 - 1/2;
            elseif F(i,k)>=3 && F(i,k)<20
                Cik(i,k)=F(i,k)/6;
            elseif F(i,k)>=20
                Cik(i,k)=3+1/3;
            end
        elseif bandas(i)>=500 && bandas(i)<=5000
            if F(i,k)>=1.5 && F(i,k)<3
                Cik(i,k)=2*F(i,k)/3-1;
            elseif F(i,k)>=3 && F(i,k)<20
                Cik(i,k)=F(i,k)/3;
            elseif F(i,k)>=20
                Cik(i,k)=6+2/3;
            end
        elseif bandas(i)>5000 && bandas(i)<=10000
            if F(i,k)>=1.5 && F(i,k)<3
                Cik(i,k)=F(i,k)/3 - 1/2;
            elseif F(i,k)>=3 && F(i,k)<20
                Cik(i,k)=F(i,k)/6;
            elseif F(i,k)>=20
                Cik(i,k)=3+1/3;
            end
        end
        
    end
end

%% Determinacion de C(k)
Ck=max(Cik);

end

