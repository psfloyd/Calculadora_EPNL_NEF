function [ calFactor ] = calc_calibracion( calFilepath, calSPL )
%CALC_CALIBRACION Calcula el factor de calibracion de un archivo y un nivel de
%   presion(SPL) de referencia
%   calFactor = calibrar(calFilepath, calSPL)
%   calFilepath: Ruta del archivo de calibracion
%   calSPL: nivel de presion grabado para la calibracion(si se omite 
%   se toma 94dB)
%   calFactor: factor de calibracion para convertir valores digitales
%   en valores de presion


if nargin < 2
   calSPL=94; 
end

% cargo el vector
calVector=audioread(calFilepath); calVector=calVector(:,1); % aseguro que sea mono
if size(calVector,1)>size(calVector,2); calVector=calVector';end


% Valor de presion del Nivel de referencia
calPresion=spl2pa(calSPL);

% Factor de calibracion
calFactor= calPresion / rms(calVector);


end

