function [ presMat ] = spl2pa( splMat )
%SPL2PA Convierte valores de SPL en presion[Pa]
%   [ presMat ] = spl2pa( splMat )
%   splMat: matriz de niveles de presion en SPL
%   presMat: matriz de valores de presion en Pa

presMat = 20e-6*10.^(splMat/20);


end

